package ccgreen.AAB;

import org.bukkit.event.block.BlockBreakEvent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;


public class AABmain extends JavaPlugin implements Listener{

	@Override
	public void onEnable(){
		getCommand("AAB").setExecutor(new commandToggle(this));
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.YELLOW + "[AAB] activated!");
	}
	
	@EventHandler
	public void OnBlockDestroy(BlockBreakEvent event){
		if(event.isCancelled()){
			return;
		}
		
		Player player = event.getPlayer(); 
		
		if(event.getBlock().getType() == Material.ANVIL){
			if(!player.hasPermission("AAB.anvilDestroy")){
				player.sendMessage(ChatColor.YELLOW + "Use /AAB to destroy anvils!");
				event.setCancelled(true);
			}
		}
	}
}