package ccgreen.AAB;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class commandToggle implements CommandExecutor {
	
	private AABmain plugin;

	public commandToggle(AABmain plugin) {
	    this.plugin = plugin;
	  }
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if (sender instanceof Player) {
			if(cmd.getName().equalsIgnoreCase("AAB")){
				Player player = (Player) sender;
				if(player.hasPermission("AAB.anvilDestroy")){
					player.sendMessage(ChatColor.RED + "Anvil destruction disabled!");
					player.addAttachment(plugin, "AAB.anvilDestroy", false);
				} else {
					player.sendMessage(ChatColor.GREEN + "Anvil destruction enabled!");
					player.addAttachment(plugin, "AAB.anvilDestroy", true);
				}
			}
	    }
	return false;
	}
}